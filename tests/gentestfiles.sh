#!/bin/sh

. ../seq.sh

# This creates the test files for test.sh, DO NOT run this on untested builds, only needed if either tests or algorithm modified.

for i in $(seq 0 1024); do ./prng $i 0 $i | ../crc64 -s; done > test1.txt
for i in $(seq 0 128); do ./prng $((1024*1024+i)) 0 $((i+1024)) | ../crc64 -s; done > test2.txt
for i in $(seq 0 1024); do echo -n "`./prng 123 0 $i | ../crc64 -s` "; echo -n "`./prng $i 123 $i | ../crc64 -s` "; echo $i; done > test3_args.txt
for i in $(seq 0 1024); do ./prng $((123+i)) 0 $i | ../crc64 -s; done > test3.txt
for i in $(seq 0 1024); do echo -n "`./prng 2048 0 $((i+1024)) | ../crc64 -s` "; echo -n "`./prng $((i*4096)) 2048 $((i+1024)) | ../crc64 -s` "; echo $((i*4096)); done > test4_args.txt
for i in $(seq 0 1024); do ./prng $((2048+i*4096)) 0 $((i+1024)) | ../crc64 -s; done > test4.txt
