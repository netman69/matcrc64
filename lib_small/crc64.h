#ifndef __CRC64_H__
#define __CRC64_H__

#include <stdint.h>
#include <stddef.h>

#define CRC64_INIT (0xFFFFFFFFFFFFFFFF)

extern uint64_t crc64(uint64_t crc, const void *data, size_t len);

#endif /* __CRC64_H__ */
