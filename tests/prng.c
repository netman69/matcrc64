#include <stdint.h>
#include <stddef.h> /* size_t */
#include <unistd.h> /* write(), STDOUT_FILENO */
#include <stdio.h> /* fprintf(), stderr */
#include <stdlib.h> /* strtoull(), strtoul(), EXIT_SUCCESS, EXIT_FAILURE */
#include <limits.h> /* strtoull(), strtoul() */

uint32_t prng_seed = 0;
uint8_t buf[4096];

uint32_t fnv1a32(uint8_t *data, size_t len) {
	uint32_t hash = 2166136261;
	while (len--)
		hash = (hash ^ *data++) * 16777619;
	return hash;
}

void prng(uint8_t *data, size_t len) {
	for (; len > 0; --len) {
		prng_seed = fnv1a32((uint8_t *) &prng_seed, sizeof(prng_seed));
		*data++ = (prng_seed >>  0) ^ (prng_seed >>  8) ^ (prng_seed >> 16) ^ (prng_seed >> 24);
	}
}

int main(int argc, char *argv[]) {
	uint64_t len, offset = 0;
	if (argc < 2 || argc > 4) {
		fprintf(stderr, "usage: %s <length> [<offset>] [<seed>]\n", argv[0]);
		return EXIT_FAILURE;
	}
	len = strtoull(argv[1], NULL, 10);
	if (argc >= 3)
		offset = strtoul(argv[2], NULL, 10);
	if (argc >= 4)
		prng_seed = strtoul(argv[3], NULL, 10);
	while (offset > 0) {
		size_t cs = ((offset > sizeof(buf)) ? sizeof(buf) : offset);
		prng(buf, cs);
		offset -= cs;
	}
	while (len > 0) {
		size_t cs = ((len > sizeof(buf)) ? sizeof(buf) : len);
		prng(buf, cs);
		write(STDOUT_FILENO, buf, cs);
		len -= cs;
	}
	return EXIT_SUCCESS;
}
