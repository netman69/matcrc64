PREFIX = /usr/local
CFLAGS += -O3 -std=c99 -Wall -pedantic -Ilib
CFLAGS_LIB = $(CFLAGS) -fpic
LDFLAGS_LIB = $(LDFLAGS) -shared -nodefaultlibs
OBJS = main.o lib/crc64.o lib/tables.o
OBJS_COMBINE = main_combine.o lib/crc64.o lib/tables.o
OBJS_SMALL = main.o lib_small/crc64.o
OBJS_LIB = lib/crc64.lib.o lib/tables.lib.o
OBJS_SLIB = lib/crc64.o lib/tables.o
OBJS_BENCH = tests/bench.o lib/crc64.o lib/tables.o
OBJS_BENCH_COMBINE = tests/bench_combine.o lib/crc64.o lib/tables.o
.SUFFIXES: .c .o .lib.o

all: crc64 crc64_combine libcrc64.so libcrc64.a tests/prng tests/time tests/bench tests/bench_combine

crc64: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

crc64_combine: $(OBJS_COMBINE)
	$(CC) $(LDFLAGS) -o $@ $(OBJS_COMBINE)

crc64_small: $(OBJS_SMALL)
	$(CC) $(LDFLAGS) -o $@ $(OBJS_SMALL)

libcrc64.so: $(OBJS_LIB)
	$(CC) $(LDFLAGS_LIB) -o $@ $(OBJS_LIB)

libcrc64.a: $(OBJS_SLIB)
	$(AR) rcs $@ $(OBJS_SLIB)

test: tests/prng crc64 crc64_combine
	cd tests; ./test.sh

tests/bench: $(OBJS_BENCH)
	$(CC) $(LDFLAGS) -o $@ $(OBJS_BENCH)

tests/bench_combine: $(OBJS_BENCH_COMBINE)
	$(CC) $(LDFLAGS) -o $@ $(OBJS_BENCH_COMBINE)

lib/tables.c: tablegen mktables.sh
	./mktables.sh > $@

lib/tables.o: lib/tables.c

.c.o: # Needed on OpenBSD because it's default one is different.
	$(CC) $(CFLAGS) -c -o $@ $<

.c.lib.o:
	$(CC) $(CFLAGS_LIB) -c -o $@ $<

clean:
	rm -f crc64 crc64_combine crc64_small libcrc64.so libcrc64.a tests/prng tests/time tests/bench tests/bench_combine tablegen lib/tables.c $(OBJS) $(OBJS_COMBINE) $(OBJS_SMALL) $(OBJS_LIB) $(OBJS_SLIB) $(OBJS_BENCH) $(OBJS_BENCH_COMBINE)

install: crc64 crc64_combine libcrc64.so libcrc64.a
	install -d $(PREFIX)/bin/
	install crc64 $(PREFIX)/bin/
	install crc64_combine $(PREFIX)/bin/
	install -d $(PREFIX)/lib/
	install libcrc64.so $(PREFIX)/lib/
	install libcrc64.a $(PREFIX)/lib/
	install -d $(PREFIX)/include/
	install -m 644 lib/crc64.h $(PREFIX)/include/
	install -d $(PREFIX)/man/man1/
	install doc/crc64.1 $(PREFIX)/man/man1/
	install doc/crc64_combine.1 $(PREFIX)/man/man1/
	install -d $(PREFIX)/man/man3/
	install doc/crc64.3 $(PREFIX)/man/man3/
	install doc/crc64.3 $(PREFIX)/man/man3/crc64_combine.3
	install doc/crc64.3 $(PREFIX)/man/man3/crc64_init.3

deinstall:
	rm -f $(PREFIX)/bin/crc64 $(PREFIX)/bin/crc64_combine $(PREFIX)/lib/libcrc64.so $(PREFIX)/lib/libcrc64.a $(PREFIX)/man/man1/crc64.1 $(PREFIX)/man/man1/crc64_combine.1 $(PREFIX)/man/man3/crc64.3 $(PREFIX)/man/man3/crc64_combine.3 $(PREFIX)/man/man3/crc64_init.3
