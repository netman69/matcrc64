seq() {
	if [ "$#" = "2" ]; then
		awk -v s=$1 -v e=$2 -v n=1 'BEGIN { for (i = s; i <= e; i += n) print i }'
	elif [ "$#" = "3" ]; then
		awk -v s=$1 -v e=$3 -v n=$2 'BEGIN { for (i = s; i <= e; i += n) print i }'
	fi
}
