#ifndef __TABLES_H__
#define __TABLES_H__

#include <stdint.h>

/* For slice-by-16 algorithm. */
extern const uint64_t crc64_tbl[][256];

/* For checksum combining. */
extern const uint64_t crc64_tbl_combine[][256];

#endif /* __TABLES_H__ */
