#!/bin/sh

. ../seq.sh

echo "# DataSize Time Speed Sum"
for n in $(seq 0 1 128); do
	echo -n "$n "
	t=$(./time)
	sum=""
	for i in $(seq 0 1000); do
		sum=$(../crc64_combine 1234566678ABCDEF DEADBEEF69696969 $n)
	done
	t=$(./time "$t")
	awk -v n=$n -v t=$t 'BEGIN { ORS=" "; t /= 1000000; print t, ((t == 0) ? 0 : (n * 100 / t)) }'
	echo "$sum"
done
