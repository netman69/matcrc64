#include "crc64.h"
#include "tables.h"
#include <stdint.h>
#include <stddef.h> /* size_t */

/* crc64
 *
 * Calculate CRC64 using slice-by-16 (and 8) algorithm.
 * Start with a crc value of CRC64_INIT (0xFFFFFFFFFFFFFFFF).
 */
uint64_t crc64(uint64_t crc, const void *data, size_t len) {
	uint8_t *ptr = (uint8_t *) data, *end = ptr + len;
	#ifdef MATCRC_ALIGN
	/* Align the pointer before processing bigger chunks. */
	while ((uintptr_t) ptr & 7 && ptr < end)
		crc = crc64_tbl[0][(crc ^ *ptr++) & 0xFF] ^ (crc >> 8);
	#endif
	/* Slice-by-16. */
	while (ptr <= end - 16) {
		crc = crc64_tbl[0][ptr[15]] ^ crc64_tbl[1][ptr[14]] ^
		      crc64_tbl[2][ptr[13]] ^ crc64_tbl[3][ptr[12]] ^
		      crc64_tbl[4][ptr[11]] ^ crc64_tbl[5][ptr[10]] ^
		      crc64_tbl[6][ptr[9]] ^ crc64_tbl[7][ptr[8]] ^
		      crc64_tbl[8][ptr[7]  ^ ((crc >> 56) & 0xFF)] ^ crc64_tbl[9][ptr[6]  ^ ((crc >> 48) & 0xFF)] ^
		      crc64_tbl[10][ptr[5] ^ ((crc >> 40) & 0xFF)] ^ crc64_tbl[11][ptr[4] ^ ((crc >> 32) & 0xFF)] ^
		      crc64_tbl[12][ptr[3] ^ ((crc >> 24) & 0xFF)] ^ crc64_tbl[13][ptr[2] ^ ((crc >> 16) & 0xFF)] ^
		      crc64_tbl[14][ptr[1] ^ ((crc >>  8) & 0xFF)] ^ crc64_tbl[15][ptr[0] ^ ((crc >>  0) & 0xFF)];
		ptr += 16;
	}
	/* Slice-by-8. */
	while (ptr <= end - 8) {
		crc = crc64_tbl[0][ptr[7] ^ ((crc >> 56) & 0xFF)] ^ crc64_tbl[1][ptr[6] ^ ((crc >> 48) & 0xFF)] ^
		      crc64_tbl[2][ptr[5] ^ ((crc >> 40) & 0xFF)] ^ crc64_tbl[3][ptr[4] ^ ((crc >> 32) & 0xFF)] ^
		      crc64_tbl[4][ptr[3] ^ ((crc >> 24) & 0xFF)] ^ crc64_tbl[5][ptr[2] ^ ((crc >> 16) & 0xFF)] ^
		      crc64_tbl[6][ptr[1] ^ ((crc >>  8) & 0xFF)] ^ crc64_tbl[7][ptr[0] ^ ((crc >>  0) & 0xFF)];
		ptr += 8;
	}
	/* Deal with remaining bytes. */
	while (ptr < end)
		crc = crc64_tbl[0][(crc ^ *ptr++) & 0xFF] ^ (crc >> 8);
	return crc;
}

/* crc64_combine
 *
 * Combine 2 of our CRCs to get the sum of the concatenated inputs for
 *   said sums. The length of the data the second sum was made from has
 *   to be given for this to work. This works by first appending len1
 *   zeros to sum1 (as if continueing to sum data but only zeros).
 * This function works very fast because the slice-by-n optimalization
 *   can be simplified for it so we need only the last 4 of the tables
 *   for a given n. To understand how this works consider the above
 *   crc64 function with zero value for all slices and the fact that
 *   all the tables start with 0. We use several tables to get an almost
 *   constant execution time regardless of the len1 value.
 */
#ifndef MATCRC_NO_COMBINE
uint64_t crc64_combine(uint64_t crc0, uint64_t crc1, uint64_t len1) {
	int bit;
	uint64_t a, b;
	/* XOR crc0 with initialization value. */
	crc0 ^= CRC64_INIT;
	/* Handle bit 5-63. */
	for (bit = 5; bit < 64; ++bit) if (len1 & ((uint64_t) 1 << bit)) {
		a = crc64_tbl_combine[bit - 5][(crc0 >> 56) & 0xFF];
		b = crc64_tbl_combine[bit - 5][(crc0 >> 48) & 0xFF];
		a ^= crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl_combine[bit - 5][(crc0 >> 40) & 0xFF];
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		a ^= crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl_combine[bit - 5][(crc0 >> 32) & 0xFF];
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		a ^= crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl_combine[bit - 5][(crc0 >> 24) & 0xFF];
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		a ^= crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl_combine[bit - 5][(crc0 >> 16) & 0xFF];
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		a ^= crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl_combine[bit - 5][(crc0 >> 8) & 0xFF];
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		a ^= crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl_combine[bit - 5][(crc0 >> 0) & 0xFF];
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		b = crc64_tbl[0][b & 0xFF] ^ (b >> 8);
		crc0 = a ^= crc64_tbl[0][b & 0xFF] ^ (b >> 8);
	}
	/* This is equivalent to slice-by-16 summing of zeros. */
	if (len1 & (1 << 4)) {
		crc0 = crc64_tbl[ 8][(crc0 >> 56) & 0xFF] ^ crc64_tbl[ 9][(crc0 >> 48) & 0xFF] ^
		       crc64_tbl[10][(crc0 >> 40) & 0xFF] ^ crc64_tbl[11][(crc0 >> 32) & 0xFF] ^
		       crc64_tbl[12][(crc0 >> 24) & 0xFF] ^ crc64_tbl[13][(crc0 >> 16) & 0xFF] ^
		       crc64_tbl[14][(crc0 >>  8) & 0xFF] ^ crc64_tbl[15][(crc0 >>  0) & 0xFF];
	}
	/* Small performance improvement is still had with slice-by-8. */
	if (len1 & (1 << 3)) {
		crc0 = crc64_tbl[0][(crc0 >> 56) & 0xFF] ^ crc64_tbl[1][(crc0 >> 48) & 0xFF] ^
		       crc64_tbl[2][(crc0 >> 40) & 0xFF] ^ crc64_tbl[3][(crc0 >> 32) & 0xFF] ^
		       crc64_tbl[4][(crc0 >> 24) & 0xFF] ^ crc64_tbl[5][(crc0 >> 16) & 0xFF] ^
		       crc64_tbl[6][(crc0 >>  8) & 0xFF] ^ crc64_tbl[7][(crc0 >>  0) & 0xFF];
	}
	/* Deal with the remaining bytes. */
	if (len1 & (1 << 2)) {
		crc0 = crc64_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
		crc0 = crc64_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
		crc0 = crc64_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
		crc0 = crc64_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
	}
	if (len1 & (1 << 1)) {
		crc0 = crc64_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
		crc0 = crc64_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
	}
	if (len1 & (1 << 0))
		crc0 = crc64_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
	return crc0 ^ crc1;
}
#endif
