#ifndef __CRC64_H__
#define __CRC64_H__

#include <stdint.h>
#include <stddef.h> /* size_t */

#define CRC64_INIT (0xFFFFFFFFFFFFFFFF)

extern uint64_t crc64(uint64_t crc, const void *data, size_t len);
extern uint64_t crc64_combine(uint64_t crc0, uint64_t crc1, uint64_t len1);

#endif /* __CRC64_H__ */
